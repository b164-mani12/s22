//data modeling case simulation

Course Booking Application

3 Data Models

1. Users - will contain the information of both customers and admin
2. Courses - will contain the courses offered.
3. Transaction - will contain the payment transaction of the customer.




=======USERS===========
UsersId
firstName
lastName
email
password
isAdmin
enrolledCourses
paymentTransaction


//schema
{
	_id: ObjectId,
	firstName: String,
	lastname: String,
	email: String,
	password: String,
	isAdmin: Boolean,
	enrolledCourses:[
		{
			courseId: CourseId,
			status: "Enrolled",
			enrolledOn: Date
		}
	],
	paymentStatus: TransactionID
}

=======Courses=======
CoursesId
name
description
price
duration
isAvailable
slots
enrollees

{
	_id: ObjectId,
	name: String,
	description: String,
	price: Number,
	duration: String,
	isAvailable: True,
	slots: Number,
	enrollees[
		{
			userId: USERID,
			enrolledOn: Date
		}
	]
}




======Transactions========
TransactionId
UserId
CourseEnrolled
price
isPaid
paymentMethod
datePayment


//transactions
{
	_id: ObjectId,
	userId: USERID,
	courseEnrolled: CourseID,
	totalPrice: Number,
	isPaid: False,
	paymentMethod: String,
	dateOfPayment: Date
}




//Document Structure
//Data Model
====Embedded Data Model======
{
	userName: "jane",
	contact: {
		mobileNo: String,
		phoneNo: String,
		email: string,
		faxNo: String
	}
}










=====Reference Data Model======
























